# Examples for Transferring (Meta)Data to Coscine

This is a collection of generic and real-world example scripts that transfer data and metadata to Coscine.

If you want to add examples, simply add a new folder to this project. 
