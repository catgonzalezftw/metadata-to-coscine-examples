Import all required libraries


```python
import flowio
import flowkit as fk
import numpy
from datetime import datetime
import coscine
import pandas as pd
import re
import FlowCal
from pathlib import Path
import json
import os
import boto3
import tempfile
from io import BytesIO
import hashlib
```

Input Coscine Access Token


```python
token=''
```

Access Coscine Project, Resource, Metadata Form


```python
%%time
client = coscine.Client(token)
print("created coscine client")

project = client.project('test_vuk_data')
assert project is not None
print("located coscine project")

resource = project.resource('test_fcs_file')
assert resource is not None
print("located project resource")

metadata = resource.metadata_form()
assert metadata is not None
print("loaded metadata form")
```
    

Input Folder Path


```python
folder_path = 'C:/Cat/graphic/Beckmann_Instrument'
```

Define Function to Obtain all .fcs and .LMD Files in the Folder Path


```python
def get_all_metadata_from_directory(folder_path):
    return_value = {}
    for pattern in ('*.fcs', '*.LMD'):
        for file_name in Path(folder_path).glob(pattern):
            sample = fk.Sample(file_name)
            fcs_metadata = sample.get_metadata()
           # # rest of the code
            return_value[file_name.as_posix()] = sample.get_metadata()

    return return_value
```


```python
metadata_files = get_all_metadata_from_directory('C:/Cat/graphic/Beckmann_Instrument')
```

Defining fcs_metadata outside of the defined function


```python
for pattern in ('*.fcs', '*.LMD'):
    for file_name in Path(folder_path).glob(pattern):
        sample = fk.Sample(file_name)
        fcs_metadata = sample.get_metadata()
```

Count number of files with the same Experiment Name in order to Obtain Number of Samples


```python
def count_exp_name_files(metadata_files):
    exp_name_count = {}
    for file_name, metadata in metadata_files.items():
        exp_name = metadata.get('experiment name')
        if exp_name is not None:
            exp_name_count[exp_name] = exp_name_count.get(exp_name, 0) + 1
        else:
            exp_name = metadata.get('groupname')
            if exp_name is not None:
                exp_name_count[exp_name] = exp_name_count.get(exp_name, 0) + 1 
            else:
                exp_name = metadata.get('experiment_name')
                if exp_name is not None:
                    exp_name_count[exp_name] = exp_name_count.get(exp_name, 0) + 1   
                else:
                    exp_name = metadata.get('@testname')
                    if exp_name is not None:
                        exp_name_count[exp_name] = exp_name_count.get(exp_name, 0) + 1 
    return sum(exp_name_count.values())
```


```python
count_exp_name_files(metadata_files)
```




    1



Set instrument Name in Coscine based on how it is stored in file metadata keys


```python
def set_instrument_name(name):
    if name == 'Aurora':
        return 'Cytek Aurora'
    else:
        if name == "FACSAriaII":
            return "BD Aria"
        else:
            if name == 'LSRFortessa':
                return 'BD LSRFortessa'
            else:
                if name == 'Gallios':
                    return 'andere'
    
    raise NotImplementedError
```


```python
fcs_metadata["cyt"] = set_instrument_name(fcs_metadata["cyt"])
```

Format time to match required field in Coscine Resource


```python
#fcs_metadata['date'] = datetime.strptime(fcs_metadata['date'],'%d-%b-%Y')

fcs_metadata['date'] = datetime.strptime(fcs_metadata['date'], '%d-%b-%y')
```

Define Function to obtain Markers Analyzed


```python
def cd4_keys(metadata_files):
    pattern = re.compile(r'^p\d+s$')    
    metadata_keys = []
    for file_path in metadata_files:
        with open(file_path, 'rb') as file:
            sample = fk.Sample(file)
            metadata = sample.get_metadata()
            metadata_keys += [key for key in metadata if pattern.match(key)]
    return metadata_keys

#metadata_files = get_all_metadata_from_directory('C:/Cat/graphic/Beckmann_Instrument')
markers = cd4_keys(metadata_files)
num_markers = len(markers)
```


```python
print(cd4_keys(metadata_files))
```

    ['p1s', 'p2s', 'p3s', 'p4s', 'p5s', 'p6s', 'p7s', 'p8s', 'p9s', 'p10s', 'p11s', 'p12s', 'p13s']
    

Input Metadata from file to Coscine


```python
metadata["Number of Samples"] = count_exp_name_files(metadata_files) 
metadata['Date of Analysis'] = fcs_metadata['date']
metadata['Creator'] = fcs_metadata['op']
metadata['Instrument'] = fcs_metadata['cyt']
metadata['Markers analysed'] = cd4_keys(metadata_files)

# Check for 'experiment name' metadata key and assign its value to 'Hypothesis' in Coscine
if 'experiment name' in fcs_metadata:
    metadata['Hypothesis'] = fcs_metadata['experiment name']
else:
    # Check for 'groupname' metadata key and assign its value to 'Hypothesis' in Coscine
    if 'groupname' in fcs_metadata:
        metadata['Hypothesis'] = fcs_metadata['groupname']
    else:
        # Check for 'experiment_name' metadata key and assign its value to 'Hypothesis' in Coscine
        if 'experiment_name' in fcs_metadata:
            metadata['Hypothesis'] = fcs_metadata['experiment_name']
        else:
        # Check for 'experiment_name' metadata key and assign its value to 'Hypothesis' in Coscine
            if '@testname' in fcs_metadata:
                metadata['Hypothesis'] = fcs_metadata['@testname']
```

Define Function to Obtain multiple files in a folder


```python
def get_all_file_paths(folder_path):
    all_file_paths = []
    for file_name in Path(folder_path).iterdir():
        if file_name.is_file():
            all_file_paths.append(file_name)
    return all_file_paths
                
all_file_paths = get_all_file_paths(folder_path)
```

Request input from user to enter Cell Source, Organism, and ORCID


```python
cell_source= input('Enter cell source: ')
organism= input('Enter organism: ')
ORCID= input('Enter your ORCID: ')
```

    Enter cell source:  liver
    Enter organism:  dog
    Enter your ORCID:  123
    

Add the user input to the Coscine Resource Metadata


```python
metadata["Cell source"] = cell_source
metadata["Organism"] = organism
metadata["ORCID"] = ORCID
```

Upload the files in the specified path to Coscine Resource


```python
for file_path in all_file_paths:
    with open(file_path, 'rb') as file:
        file_name = file_path.name
        resource.upload(file_name, file, metadata)
```


    HEL 72h Ruxo release 20221110 CD84 DMSO 00018993 316.LMD:   0%|          | 0.00/1.60M [00:00<?, ?B/s]



```python

```


```python

```
