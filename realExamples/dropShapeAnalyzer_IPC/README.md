# Transferring Drop Shape Analyzer (Meta)Data to Coscine with eLabFTW

The python script `data_to_coscine_S3.py` transfers raw and processed data files stored on a local hard drive to separate resources in Coscine.

## Coscine Setup

This script was written with two RDS-S3 resources in mind, one for raw and one for calculated data. these should be located withtin the same Coscine project. For example, all data belonging to SFB 985 should have two resources in the Coscine SFB 985 project (or related sub-project). Other data should have it's own project and the two resources contained within that project. Please ensure that at least one project PI has been invited to the project.


Resource naming:
Name the raw data resource and then assign the same resource name for the processed data and add `calc` to the end. For example:

- raw data resource name: Drop Shape Analyzer
- processed data resource name: Drop Shape Analyzer_calc

*Note: If necessary, the script should run without a problem for a single resource.*

For each resource, select the DropShapeAnalysis application profile (located under SFB 985). *Please note, this application profile may also be selected for projects outside SFB 985*

This script requires a Coscine token save in the following file:

[config.json](config.json)

This token is generated in the [user profile](https://coscine.rwth-aachen.de/user/) on Coscine. This token does expire; please ensure that it is kept up to date. 

Do not publicly share this token and ensure the [config.json](config.json) file is stored securely.

## eLabFTW Setup

This Python script uses the eLabFTW API, see the [documentation](https://doc.elabftw.net/api.html) for further info.

Add the following items to the [database](https://doc.elabftw.net/user-guide.html?highlight=database#database):

- Resources: add the data resources with the exact *display* names that they have been given in Coscine. (Include Coscine link and PID here for easy reference.)

- Projects: add the Coscine projects with the exact *display* names they have been given in Coscine. (Include Coscine link here for easy reference.)

- Device: Ensure your device is added to the database. Atd the exact name of the device in eLabFTW to the [config.json](config.json) file. 

Setup an experiment [template](https://doc.elabftw.net/user-guide.html?highlight=database#database) which includes the metadata template as follows (with default and list values adjusted accordingly):

```json
{
    "extra_fields": {
      "Person Responsible": {
        "type": "select",
        "value": " ",
        "position": 1,
        "options": [
          " ",
          "WiMi 1",
          "WiMi 2"
        ]
      },
      "Operator": {
        "type": "select",
        "position": 2,
        "value": " ",
        "options": [
          " ",
          "User 1",
          "User 2"
        ]
      },
      "Sample ID": {
        "type": "text",
        "position": 3
      },
      "SFB 985 Sample Management PID": {
        "type": "text",
        "position" : 4
      },
      "Instrument": {
        "type": "select",
        "value": " ",
        "position": 5,
        "options": [
          " ",
          "Krüss DSA100"
        ]
      },
      "Accessories": {
        "type": "select",
        "value": " ",
        "position": 8,
        "allow_multi_values": true,
        "options": [
          " ",
          "Oscillating drop module (DS3270)",
          "Temperature-controlled glass cuvette (SC30)",
          "3D-printed chamber for photoresponsive materials"
        ]
      },
      "Type of capillary tip": {
        "type": "radio",
        "position": 9,
        "options": [
          "NE45 (d=1.8mm)",
          "NE44 (d=0.5mm)"
        ]
      }, 
      "Measurement Type": {
        "position": 10,
        "type": "radio",
        "options": [
          "pendant drop",
          "oscillating drop",
          "sessile drop"
        ]
      },
      "Oscillating drop measurement type (if applicable)": {
        "type": "select",
        "value": " ",
        "position": 11,
        "options": [
          " ",
          "amplitude sweep",
          "frequency sweep",
          "temperature sweep",
          "oscillation during adsorption"
        ]
      },
      "Sample Concentration [wt%]": {
        "type": "number",
        "position": 12
      },  
      "Interface type": {
        "type": "radio",
        "position": 13,
        "options": [
          "air-water",
          "oil-water"
        ]
      },  
      "Type of oil (if applicable)": {
        "type": "select",
        "value": " ",
        "position": 14,
        "options": [
          " ",
          "n-decane"
        ]
      },  
      "Additional notes": {
        "position": 15,
        "type": "text"
      }
    }
  }

```

For more info, see the eLabFTW [documentation](https://doc.elabftw.net/metadata.html). You can setup multiple templates to fit metadata needs. For example, if one operator has a typical set of preset values, this could be set in their own template.

[Link](https://doc.elabftw.net/user-guide.html?highlight=database#linked-items) the device, the Coscine project, and the Coscine resources in the template. If data is to be uploaded to various Coscine projects or resources, it will be beneficial to setup multiple experiment templates and link the corresponding projects and resources within.

Generate a token for the eLabFTW API and save in [config.json](config.json). You can generate these in your user panel --> API Keys in eLabFTW. Choose read/write access. *Note: Currently, only read access is used, but the script may be modified in the future to write information on upload status to the experiments.*

Include your eLabFTW API endpoint in the [config.json](config.json) as well.

When running experiments, setup all metadata and ensure the correct device, Coscine project, and Coscine resources are linked. When the experiment has successfully completed, set this in the eLabFTW experiment as well. Only data belonging to experiments marked 'SUCCESS' will be transferred to Coscine.

## Python Script Setup

You need Python 3 and the following packages installed to run the script, e.g., using Python's package manager [pip](https://pip.pypa.io/en/stable/installation/):

- elabpy
- coscine (tested on V0.8.1)

The remaining packages (imported at the beginning of the script) should be included in Python. If this is not the case, they may also be installed using pip.

As indicated above, above, a [config.json](config.json) file is required. It contains the following:

```json
{
    "token_cos" : "YOUR TOKEN",
    "token_elab" : "YOUR TOKEN",
    "ENDPOINT_elab" : "YOUR ELAB INSTANCE API ENDPOINT", 
    "data_folder" : "YOUR DATA FOLDER PATH",
    "device" : "Tensiometer, Drop Shape Analyzer (R 105)",
    "report_folder" : "uploadReports",
    "reportinfo_name" : "reportinfo.json", 
    "LOG_FOLDER" : "logs",
    "log_file_name" : "DSA_script_error.log"
}
```

Specify the data location in [config.json](config.json). The script assumes all data is saved within on fop-level folder and will search through any sub-folders within this spcified folder. It assumes raw data is in `*.zip` format and calculated data is in `*.xlsx` format. 

The script writes monthly log files in JSON format (could also be modified for PDF, but JSON is nice for machine-readability). It requires a JSON file named [reportinfo.json](reportinfo.json) with the following content:
```json
{
  "uploaded": [], 
  "Experiments missing calculated data": [], 
  "Experiments missing raw data": [], 
  "Experiments missing metadata for raw data": [], 
  "Experiments missing metadata for calculated data": []
}
```
Specify the report folder in the [config.json](config.json) file. 

The script should be setup to run periodicially, for example once a day at night. As it sends multiple requests to eLabFTW, it can hinder the ELN's performance. Thus, runnig it continuously or when many people are actively working is not ideal. For example, use [task scheduler for this in Windows](https://www.jcchouinard.com/python-automation-using-task-scheduler/). 

It will write exceptions to a log file titles `logs/DSA_script_error.log`, located in the `logs` directory as defined in within the directory containing this script [config.json](config.json). 