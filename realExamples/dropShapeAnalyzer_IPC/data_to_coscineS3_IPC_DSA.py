###############################################################################
# Drop Shape Analisys Data Transfer to Coscine
###############################################################################
# Copyright (c) 2022 RWTH Aachen University
# Public Domain MIT License
# Authors: N. Parks | SFB985
###############################################################################
'''
This script transfers raw and calculated from the KRÜSS Drop Shape
Analyzer to dedicated resources i n a project in Coscine.
Successful experiments in eLabFTW trigger the transfer. Metadadata are 
collected from eLabFTW and the raw data file and transferred as well.
'''
###############################################################################
# Dependencies
###############################################################################
import requests
# this is needed to avoid SSL vertificate errors
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'DEFAULT:!DH'
# from pickle import TRUE
from zipfile import ZipFile
import xml.etree.ElementTree as ET
import elabapy
import coscine
import json
import os
import sys
import logging
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from datetime import datetime
###############################################################################
# Setup
###############################################################################
# load the config file
config_path = Path('.') / Path('config.json')
with open(config_path, "rt", encoding="utf-8") as fp:
    cfg = json.load(fp)

DEVICE: str = cfg['device']
ENDPOINT_ELAB : str = cfg['ENDPOINT_elab']
TOKEN_COS: str = cfg['token_cos']
TOKEN_ELAB: str = cfg['token_elab']

LOG_FOLDER = Path(cfg["LOG_FOLDER"])
LOG_FILE = LOG_FOLDER / cfg['log_file_name']

local_path = Path(cfg['data_folder'])
report_folder = Path(cfg['report_folder']) 
reportinfo_name = Path(cfg['reportinfo_name']) 

# setup logger
logger = logging.getLogger("coscine-upload")
logger.setLevel(logging.INFO)
formatter = logging.Formatter(
    "%(asctime)s — %(name)s — %(levelname)s — %(message)s"
)
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)
file_handler = TimedRotatingFileHandler(LOG_FILE, when='midnight')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
###############################################################################
def getElabExpInfo(device_name, expID):
    '''
    Checks if an eLabFTW experiment pertains to the device and, if it does, 
        returns the full experiment.

    Params: 
        device_name: name of the device. Must match the data base item name 
            in eLabFTW. (string)
        expID: experiment ID in elabftw. NOT the unique ID. The ID used 
            in the experiment URL. (int)
    
    Returns: 
        experiment: all experiment information (dict) 
        or None
    '''
    experiment = manager.get_experiment(expID)
    if [link for link in experiment['items_links'] 
        if device_name in link.values()]: 
        return experiment 
###############################################################################
def dataInResource(resource, exp_title, paths):
    '''
    Checks if there is data in a given Coscine resource 
    for a given experiment in Coscine

    Params:
    resource: Coscine resource 
    exp_title: expeiment title from elabFTW

    Returns:
    True/False wheter data is in Coscine resource (boolean)
    '''
    files = resource.objects()
    for file in files:
        if exp_title in file.name:

            if file.is_folder and len(resource.objects(file.name + "/")) != len(paths):
                return False
            return True
    return False
###############################################################################
def metadataInResource(resource, exp_title):
    '''
    Checks if there is metadata in a given Coscine resource 
    for a given experiment in Coscine.

    Params:
    resource: Coscine resource 
    exp_title: expeiment title from elabFTW (str)

    Returns:
    check True/False whether metadata is in Coscine (boolean)
    '''
    
    files = resource.objects()
    check = False
    for file in files:
        if exp_title in file.name:
            check = file.has_metadata
            if file.is_folder:
                files_in_folder = resource.objects(file.name + "/")
                for f in files_in_folder:
                    check = f.has_metadata
            return check
###############################################################################
def fillCosMetadata(resource, exp, path_raw):
    '''
    Fills in coscine metadata form. 
    Uses metadata entered into an eLabFTW experiment.
    Calls embeddedMetadata function to get metadata embedded in 
        exported raw data file. 
    This is specific to DSA experiments using the Krüss Drop Shape Analyzer 
    with the template in the IPC's istance of eLabFTW.

    Params:
        resource: Coscine resource that contains the data.
        exp: elbaftw experiment to which the (meta)data belongs
        path_raw: raw data local file path. If this does not exist, 
            default value for extracted metadata is entered.
    
    Returns:
        metadata: filled in Coscine metadata form. 
    '''
    # handleded as list in Coscine
    # get the elabftw metadata
    elabmd = json.loads(exp['metadata'])['extra_fields']
    # get the coscine metatdata form for the resource
    metadata = resource.metadata_form() # empty form
    # cleanup elabftw metadata to fit coscine
    #accessories can have multiple entries, coscine takes this as a list
    accessories = [] 
    for key in elabmd:
        # multi-select now possible in elabftw
        # but in case experiments use old templae, leave workaround in
        try:
            if 'Accessory' in key:
                if elabmd[key]['value'] != ' ':   
                    accessories.append(elabmd[key]['value'])
            elif ' (if applicable)' in key:
                cos_key = key.replace(' (if applicable)', '')
                metadata[cos_key]=elabmd[key]['value']
            
            # elabftw returns strings, 
                # coscine won't accept nume values as str
            # there's definitely a more elegant way to do this...
            else:
                try:    
                    metadata[key]=elabmd[key]['value']
                    
                except TypeError:
                    try:
                        metadata[key]=float(elabmd[key]['value'])
                    except TypeError:
                        metadata[key]=int(elabmd[key]['value'])
        
        except KeyError: # when nothing entered, it has no value key
            continue
        except ValueError: 
            continue #skip lists that have no value 

    try:
        metadata['ADVANCE software version'] = embeddedMetadata(path_raw)
    except Exception as e:
        metadata['ADVANCE software version'] = '1.12'
    
    metadata['Date of Measurement'] = datetime.strptime(
        exp['date'], '%Y-%m-%d')
    
    if accessories: # old extra fields (no multi-select)
        metadata['Accessories'] = accessories
    
    metadata['Experiment ID (eLabFTW)'] = exp['elabid'] 
    
    return metadata 
###############################################################################
def embeddedMetadata(path_raw):
    '''
    Extracts metadata from an exported Küss DSA raw data file 
        (specific to this method).
    Currently only extracts software version (maybe add more later?)

    Params:
        path_raw: raw data file local path
    Returns:
        software_version: KRÜSS software version used in the measurement.
    '''

    # opening the zip file in READ mode
    with ZipFile(path_raw, 'r') as zip:
        
        # find and extract the xml file
        for name in zip.namelist():
            if 'xml' in name:
                XMLfile_name = name

        zip.extract(XMLfile_name)

    # Pass the path of the xml document to enable the parsing process
    tree = ET.parse(XMLfile_name)
    
    # get the parent tag of the xml document
    root = tree.getroot()
    # get the version number
    for version_no in root.iter('VersionNumber'):
        software_version = version_no.text

    # delete the extracted xml file (just needed it for version number)
    os.remove(XMLfile_name)
    return software_version
###############################################################################
def localData(exp_title, data_path, ext):
    '''
    Creates a list of paths that contain the name of the experiment 
    and have a given extention in a given file path.

    Params:
        exp_title: title of the eLabFTW experiment (str)
        data_path: local data folder path (pathlib path) 
        ext: file extention (str)
    
    Returns:
        files: list of file pathlib paths to the data files (list)
        
    '''
    files = [p for p in data_path.rglob(exp_title + "*" + ext)]
    return files # list of paths
###############################################################################
def S3Folder(exp_title, paths, resource):
    '''
    Creates folder in given Coscine resource accoridng to provided experiment 
    name and uploads data files in given list of paths via S3.

    Params:
        exp_title: experiment title in elabftw (str)
        paths: list of pathlib paths for data to be uploaded (list)
        resource: Coscine RDS-S3 to which to upload the data 

    Returns:
        True or False indicating whether data has been uploaded (Boolean)
    '''
    # create folder in coscine 
    resource.s3.mkdir(exp_title + '/')

    for path in paths:
        cos_path = exp_title + '/' + path.name
        resource.s3.upload(cos_path, str(path)) 

    return dataInResource(resource, exp_title, paths)
###############################################################################
def S3File (exp_title, path, resource):
    '''
    Uploads data file in the given path to a given Coscine resource via S3.

    Params:
        path: path for data to be uploaded (pathlib path)
        resource: Coscine RDS-S3 to which to upload the data 

    Returns:
        True or False indicating whether data has been uploaded (Boolean)
    '''

    resource.s3.upload(path.name, str(path))
 
    return dataInResource(resource, exp_title, path)
###############################################################################
def uploadCoscineS3(resource, paths, exp_title): 
    '''
    Gets S3 access info via Coscine API and passes this as well as the 
    paths ("folder" upload) or path (file upload) to other functions 
    to upload the data via S3.  
    
    Params:
        resource: Coscine RDS-S3 resoure to upload the data to
        paths: list of path to the local files to be uploaded (
            list of pathlib path)
        exp_title: title of the experiment in elabftw (str)
    
    Return:
        uploaded: True of False whether the data was uploaded sucessfully (
            boolean)
    '''

    if len(paths) > 1:
        uploaded = S3Folder(exp_title, paths, resource)
    else:
        uploaded = S3File(exp_title, paths[0], resource)

    return uploaded 
###############################################################################
def metadataToCos(resource, experiment, metadata):
    '''
    Checks if there is metadata in a given Coscine resource 
    for a given experiment in Coscine.

    Params:
        resource: Coscine resource 
        exp_title: expeiment title from elabFTW (str)
        metadata: filled in coscine metatdata form or dict 
    Returns:
        True/False depending whether metdata successfully updated (boolean)
    '''

    files = resource.objects()
    for file in files:
        if experiment['title'] in file.name:
            file.update(metadata)
            if file.is_folder:
                files_in_folder = resource.objects(file.name + "/")
                for f in files_in_folder:
                    f.update(metadata)
                return True
    
    return False
###############################################################################
def reportInfo(title, category, filename):
    '''
    Adds info to the report info JSON file.

    Params:
        title: experiment title (str)
        category: category, matches report key (str)
        filename: report info file name
    '''
    with open(filename, "r") as jsonFile:
        report_info = json.load(jsonFile)

    if title not in report_info[category]:
        report_info[category].append(title)

        with open(filename, "w") as jsonFile:
            json.dump(report_info, jsonFile)
###############################################################################
def writeJSONreport(reportinfoname, report_folder):
    '''
    Takes info from reportinfo and writes a monthly report 
        in a folders sorted by year.

    Params:
        reportinfoname: JSON report info file name (str)
        report_folder: name of folder that contains the reports (str)
    '''

    today = datetime.now() 
        
    report_subfolder = report_folder / str(today.year) 
    report_subfolder.mkdir(parents=True, exist_ok=True)
        
    p = report_subfolder.glob('*.json')
    paths = [x for x in p if today.strftime("%Y-%m") in str(x)]

    if len(paths) == 0: # if nothing for this month exists
        report_path = report_subfolder / '{}_report.json'.format(
            today.strftime("%Y-%m"))
        # get the uploaded exp info from the reportinfo json file
        with open(reportinfoname, "r") as jsonFile:
            report_info = json.load(jsonFile)
    
        report_info['report date'] = today.strftime("%Y-%m-%d")

        with open(report_path, 'w') as jsonFile:
            json.dump(report_info, jsonFile)
        
        # clear original json file for next report
        for item in report_info:
            report_info[item] = []
        with open(reportinfoname, "w") as jsonFile:
            json.dump(report_info, jsonFile) 


###############################################################################
# Main
###############################################################################
try:
    # API Clients
    client_settings = coscine.Settings(verbose=True)
    client = coscine.Client(TOKEN_COS, settings=client_settings)
    manager = elabapy.Manager(endpoint=ENDPOINT_ELAB, token=TOKEN_ELAB)

    succ_exp = [exp for exp in manager.get_all_experiments(
        params = {'limit':10000}) if exp['category']  == 'Success']

    for exp in succ_exp:
        # see if experiment matches device
        experiment = getElabExpInfo(DEVICE, exp['id'])
        if experiment:
            # get coscine info from experiment
            PROJECT = [item['title'] for item in experiment[
                'items_links'] if item['category']=='coscine: project'][0]
            
            project = client.project(PROJECT, toplevel=False)
            
            resources = [item['title'] for item in experiment[
                'items_links'] if item['category']=='coscine: resource']
            
            
            for res in resources:
                resource = project.resource(res)
                if 'calc' in res:
                    paths = localData(
                        experiment['title'], local_path, '.xlsx')
                    data_type = 'calculated'
                else:
                    paths = localData(
                        experiment['title'], local_path, '.zip')
                    data_type = 'raw'
                if not dataInResource(resource, experiment['title'], paths):
                    
                    if paths:
                        if uploadCoscineS3(
                            resource, paths, experiment['title']): 
                            reportInfo(
                                exp['title'], 'uploaded', 'reportinfo.json')
                    else: 

                        cat = "Experiments missing {} data".format(data_type)
                        reportInfo(exp['title'], cat, 'reportinfo.json')
                if dataInResource(
                    resource, experiment['title'], paths) and not (
                        metadataInResource(resource, experiment['title'])):
                    
                    metadata = fillCosMetadata(resource, experiment, paths)
                    metadataToCos(resource, experiment, metadata)
                else: 
                    cat = "Experiments missing metadata for {} data".format(
                        data_type)
                    reportInfo(exp['title'], cat, 'reportinfo.json')
    
    writeJSONreport(reportinfo_name, report_folder)
except Exception as e:
    logger.exception(e)
###############################################################################